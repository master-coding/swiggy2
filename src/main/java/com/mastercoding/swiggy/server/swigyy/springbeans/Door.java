package com.mastercoding.swiggy.server.swigyy.springbeans;

import org.springframework.stereotype.Component;

@Component
public class Door {
    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "Door{" +
                "name='" + name + '\'' +
                '}';
    }
}
