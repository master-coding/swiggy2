package com.mastercoding.swiggy.server.swigyy;

import com.mastercoding.swiggy.server.swigyy.beans.Restaurant;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Time;

public class RestaurantMapper implements RowMapper<Restaurant> {
    @Override
    public Restaurant mapRow(ResultSet result, int rowNum) throws SQLException {
        String name = result.getString("name");
        String area = result.getString("area");
        Time openTime = result.getTime("open_time");
        String openTimeStr = openTime != null ? openTime.toString() : "";
        Time closeTime = result.getTime("close_time");
        String closeTimeStr = closeTime != null ? closeTime.toString() : "";
        String country = result.getString("country");

        Restaurant restaurant = new Restaurant();
        restaurant.setName(name);
        restaurant.setArea(area);
        restaurant.setOpenTime(openTimeStr);
        restaurant.setCloseTime(closeTimeStr);
        restaurant.setCountry(country);
        return restaurant;
    }
}
