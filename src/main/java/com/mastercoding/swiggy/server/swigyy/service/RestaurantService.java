package com.mastercoding.swiggy.server.swigyy.service;

import com.mastercoding.swiggy.server.swigyy.RestaurantMapper;
import com.mastercoding.swiggy.server.swigyy.beans.Restaurant;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class RestaurantService {
    @Autowired
    JdbcTemplate jdbcTemplate;


    @Autowired
    NamedParameterJdbcTemplate namedParameterJdbcTemplate;

    public List<Restaurant> getRestaurants() {
        List<Restaurant> restaurants = jdbcTemplate.query("select * FROM swiggy.restaurant", new RestaurantMapper());
        return restaurants;
    }

    public Restaurant getRestaurant(String name){
        Restaurant restaurant = jdbcTemplate.queryForObject("select * FROM swiggy.restaurant where name = ?", new Object[]{name}, new RestaurantMapper());
        return restaurant;
    }

    public Restaurant getRestaurant1(String name){
        MapSqlParameterSource namedParameters = new MapSqlParameterSource();
        namedParameters.addValue("restName", name);
        Restaurant restaurant = namedParameterJdbcTemplate.queryForObject("select * FROM swiggy.restaurant where name = :restName", namedParameters, new RestaurantMapper());
        return restaurant;
    }
}
