package com.mastercoding.swiggy.server.swigyy;

import com.mastercoding.swiggy.server.swigyy.beans.Restaurant;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

@RestController
public class RestaurantController {

    @Value("${mastercoding.swiggy.jdbc.url}")
    private String url;
    
    @Value("${mastercoding.swiggy.jdbc.driver}")
    private String driver;

    @Value("${mastercoding.swiggy.jdbc.username}")
    private String userName;

    @Value("${mastercoding.swiggy.jdbc.password}")
    private String password;

    @RequestMapping(value = "/restaurants", method = RequestMethod.PUT)
    ResponseEntity<String> addOrUpdateRestaurant(@RequestBody Restaurant restaurant) throws ClassNotFoundException, SQLException {
        Class.forName(driver);
        int numRows = 0;
        try(Connection connection = DriverManager.getConnection(url, userName, password);
        Statement statement = connection.createStatement()) {
            ResultSet result = statement.executeQuery("select * from swiggy.restaurant where name = '" + restaurant.getName() + "'");
            if (result.next()) {
                //the record exists with the name
                String query = "update swiggy.restaurant set "
                        + "area = '" + restaurant.getArea() + "',"
                        + "open_time = '" + restaurant.getOpenTime() + "',"
                        + "close_time = '" + restaurant.getCloseTime() + "',"
                        + "country ='" + restaurant.getCountry() + "' "
                        + "where name = '" + restaurant.getName() + "'";
                numRows = statement.executeUpdate(query);
            } else {
                //the record is not available
                String query = "insert into swiggy.restaurant(name, area, open_time, close_time, country) values("
                        + "'" + restaurant.getName() + "',"
                        + "'" + restaurant.getArea() + "',"
                        + "'" + restaurant.getOpenTime() + "',"
                        + "'" + restaurant.getCloseTime() + "',"
                        + "'" + restaurant.getCountry() + "')";
                numRows = statement.executeUpdate(query);
            }
        }

        return new ResponseEntity<>(numRows + " row is inserted/updated", HttpStatus.CREATED);
    }

    @RequestMapping(value = "/restaurants", method = RequestMethod.GET)
    ResponseEntity<List<Restaurant>> getRestaurants() throws ClassNotFoundException, SQLException {
        List<Restaurant> restaurants = new ArrayList<>();

        Class.forName(driver);

        Connection connection = null;
        Statement statement = null;
        try {
            connection = DriverManager.getConnection(url, userName, password);
            statement = connection.createStatement();
            ResultSet result = statement.executeQuery("select * from swiggy.restaurant");

            while (result.next()) {
                String name = result.getString("name");
                String area = result.getString("area");
                Time openTime = result.getTime("open_time");
                String openTimeStr = openTime != null ? openTime.toString() : "";
                Time closeTime = result.getTime("close_time");
                String closeTimeStr = closeTime != null ? closeTime.toString() : "";
                ;
                String country = result.getString("country");

                Restaurant restaurant = new Restaurant();
                restaurant.setName(name);
                restaurant.setArea(area);
                restaurant.setOpenTime(openTimeStr);
                restaurant.setCloseTime(closeTimeStr);
                restaurant.setCountry(country);
                restaurants.add(restaurant);
            }
        }finally {
            if(statement != null){
                statement.close();
            }
            if(connection != null){
                connection.close();
            }
        }

        return new ResponseEntity<>(restaurants, HttpStatus.OK);

    }

    //fetch restaurant by name
    @RequestMapping(value = "/restaurants/{pathName}/", method = RequestMethod.GET)
    ResponseEntity<Restaurant> getRestaurant(@PathVariable("pathName") String name) throws ClassNotFoundException, SQLException {
        Class.forName(driver);

        Restaurant restaurant = new Restaurant();
        //try with resources
        try(Connection connection = DriverManager.getConnection(url, userName, password);
        Statement statement = connection.createStatement()) {
            ResultSet result = statement.executeQuery("select * from swiggy.restaurant where name = '" + name + "'");

            while (result.next()) {
                String area = result.getString("area");
                String openTime = result.getTime("open_time").toString();
                String closeTime = result.getTime("close_time").toString();
                String country = result.getString("country");

                restaurant.setName(name);
                restaurant.setArea(area);
                restaurant.setOpenTime(openTime);
                restaurant.setCloseTime(closeTime);
                restaurant.setCountry(country);
                break;
            }
        }

        return new ResponseEntity<>(restaurant, HttpStatus.OK);
    }

    //delete restaurant by name
    @RequestMapping(value = "/restaurants/{pathName}/", method = RequestMethod.DELETE)
    ResponseEntity<String> deleteRestaurant(@PathVariable("pathName") String name) throws ClassNotFoundException, SQLException {
        Class.forName(driver);
        try(Connection connection = DriverManager.getConnection(url, userName, password);
        Statement statement = connection.createStatement()) {

            ResultSet result = statement.executeQuery("select * from swiggy.restaurant where name = '" + name + "'");
            if (result.next()) {
                String query = "delete from swiggy.restaurant where name = "
                        + "'" + name + "'";
                statement.executeUpdate(query);
            }
        }
        return new ResponseEntity<>(name + " is deleted", HttpStatus.OK);
    }

}
