package com.mastercoding.swiggy.server.swigyy;

import com.mastercoding.swiggy.server.swigyy.springbeans.Employee;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan(basePackages = {"com.mastercoding.swiggy.server"})
public class SwigyyApplication {
	public static void main(String[] args) {
		ApplicationContext context = SpringApplication.run(SwigyyApplication.class, args);
		Employee employee = context.getBean(Employee.class);
		employee.setName("RAM");
		System.out.println(employee);

		Employee employee1 = context.getBean(Employee.class);
		System.out.println(employee1);

	}
}
