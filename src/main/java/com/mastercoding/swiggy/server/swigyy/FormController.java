package com.mastercoding.swiggy.server.swigyy;

import com.mastercoding.swiggy.server.swigyy.beans.Person;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
public class FormController {
    @RequestMapping(value = "/submit", method = RequestMethod.POST)
    ResponseEntity<String> submit(@ModelAttribute Person person) {
        String details = person.getName()
                + " : "
                + person.getGender()
                + " : "
                + person.getCountry()
                + " : "
                + person.getStates()
                + " : "
                + person.getAddress1()
                + " : "
                + person.getAddress2()
                + " : "
                + person.getCaste();
        return new ResponseEntity<>(details, HttpStatus.OK);
    }
}
