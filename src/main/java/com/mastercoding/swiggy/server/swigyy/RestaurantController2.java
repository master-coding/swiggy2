package com.mastercoding.swiggy.server.swigyy;

import com.mastercoding.swiggy.server.swigyy.beans.Restaurant;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.web.bind.annotation.*;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

@RestController
public class RestaurantController2 {

    @Autowired
    JdbcTemplate jdbcTemplate;

    @Autowired
    NamedParameterJdbcTemplate namedParameterJdbcTemplate;

    @RequestMapping(value = "/restaurants2", method = RequestMethod.GET)
    ResponseEntity<List<Restaurant>> getRestaurants() throws ClassNotFoundException, SQLException {
        List<Restaurant> restaurants = jdbcTemplate.query("select * FROM swiggy.restaurant", new RestaurantMapper());
        return new ResponseEntity<>(restaurants, HttpStatus.OK);
    }

    @RequestMapping(value = "/restaurants2/{pathName}/", method = RequestMethod.GET)
    ResponseEntity<Restaurant> getRestaurant(@PathVariable("pathName") String name) throws ClassNotFoundException, SQLException {
        Restaurant restaurant = jdbcTemplate.queryForObject("select * FROM swiggy.restaurant where name = ?", new Object[]{name}, new RestaurantMapper());
        return new ResponseEntity<>(restaurant, HttpStatus.OK);
    }

    @RequestMapping(value = "/restaurants21/{pathName}/", method = RequestMethod.GET)
    ResponseEntity<Restaurant> getRestaurant1(@PathVariable("pathName") String name) throws ClassNotFoundException, SQLException {
        MapSqlParameterSource namedParameters = new MapSqlParameterSource();
        namedParameters.addValue("restName", name);
        Restaurant restaurant = namedParameterJdbcTemplate.queryForObject("select * FROM swiggy.restaurant where name = :restName", namedParameters, new RestaurantMapper());
        return new ResponseEntity<>(restaurant, HttpStatus.OK);
    }

}