package com.mastercoding.swiggy.server.swigyy.springbeans;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class Home {
    private Door door;

    public Home(Door door){
        this.door = door;
    }

    @Override
    public String toString() {
        return "Home{" +
                "door=" + door +
                '}';
    }
}
