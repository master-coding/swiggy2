package com.mastercoding.swiggy.server.swigyy.controller;

import com.mastercoding.swiggy.server.swigyy.RestaurantMapper;
import com.mastercoding.swiggy.server.swigyy.beans.Restaurant;
import com.mastercoding.swiggy.server.swigyy.service.RestaurantService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.sql.SQLException;
import java.util.List;

@RestController
public class RestaurantController3 {

    @Autowired
    RestaurantService restaurantService;

    @RequestMapping(value = "/restaurants3", method = RequestMethod.GET)
    ResponseEntity<List<Restaurant>> getRestaurants() throws ClassNotFoundException, SQLException {
        return new ResponseEntity<>(restaurantService.getRestaurants(), HttpStatus.OK);
    }

    @RequestMapping(value = "/restaurants3/{pathName}/", method = RequestMethod.GET)
    ResponseEntity<Restaurant> getRestaurant(@PathVariable("pathName") String name) throws ClassNotFoundException, SQLException {
        return new ResponseEntity<>(restaurantService.getRestaurant(name), HttpStatus.OK);
    }

    @RequestMapping(value = "/restaurants31/{pathName}/", method = RequestMethod.GET)
    ResponseEntity<Restaurant> getRestaurant1(@PathVariable("pathName") String name) throws ClassNotFoundException, SQLException {
        return new ResponseEntity<>(restaurantService.getRestaurant1(name), HttpStatus.OK);
    }

}