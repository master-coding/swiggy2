package com.mastercoding.swiggy.server;

import com.mastercoding.swiggy.server.swigyy.springbeans.Door;
import com.mastercoding.swiggy.server.swigyy.springbeans.Home;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class MyConfig {
    @Bean
    Home getHome(Door door){
        door.setName("Glass Door");
        return new Home(door);
    }
}
