package com.mastercoding.swiggy.server;

import com.mastercoding.swiggy.server.swigyy.beans.Human;
import com.mastercoding.swiggy.server.swigyy.springbeans.Home;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
public class WelcomeController {

    @Autowired
    Home home;
    // http://localhost:8080/welcome
    @RequestMapping(value = "/welcome", method = RequestMethod.GET)
    String welcome() {
        System.out.println(home);
        return "Welcome to Spring Boot";
    }

    // http://localhost:8080/welcome1?name=LAKSHMAN&area=HYD
    @RequestMapping(value = "/welcome1", method = RequestMethod.GET)
    String welcome1(@RequestParam("name") String personName, @RequestParam("area") String area, @RequestParam("country") String country1) {
        return "Welcome " + personName + ": " + area + ":" + country1 + " to Spring Boot";
    }

    @RequestMapping(value = "/welcome2", method = RequestMethod.POST)
    ResponseEntity<String> welcomePost(@RequestBody Human human){
        String finalValue = "Welcome " + human.getName() + " : " + human.getArea() + " : " + human.getCountry() + " to Srping Boot";
        return new ResponseEntity<>(finalValue, HttpStatus.OK);
    }

    @RequestMapping(value = "/welcome2", method = RequestMethod.PUT)
    ResponseEntity<String> welcomePut(@RequestBody Human human){
        String finalValue = "Welcome " + human.getName() + " : " + human.getArea() + " : " + human.getCountry() + " to Srping Boot";
        return new ResponseEntity<>(finalValue, HttpStatus.OK);
    }

    @RequestMapping(value = "/welcome2", method = RequestMethod.PATCH)
    ResponseEntity<String> welcomePatch(@RequestBody Human human){
        String finalValue = "Welcome " + human.getName() + " : " + human.getArea() + " : " + human.getCountry() + " to Srping Boot";
        return new ResponseEntity<>(finalValue, HttpStatus.OK);
    }

    @RequestMapping(value = "/welcome2", method = RequestMethod.DELETE)
    ResponseEntity<String> welcomeDelete(@RequestParam("name") String name){
        return new ResponseEntity<>(name + " is deleted", HttpStatus.OK);
    }

    @RequestMapping(value = "/welcome2", method = RequestMethod.GET)
    ResponseEntity<Human> welcomeGet(){
        Human human = new Human();
        human.setName("LAKSHMAN");
        human.setArea("Bangalore");
        human.setCountry("India");
        return new ResponseEntity<>(human, HttpStatus.OK);
    }

}
